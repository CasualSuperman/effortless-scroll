export const PROCESSED_POST_CLASS = "effortless-post";
export const UNPROCESSED_SELECTOR = `:scope > :not(.${PROCESSED_POST_CLASS})`;
export const PROCESSED_SELECTOR = `:scope > .${PROCESSED_POST_CLASS}`;

const translations: WeakMap<HTMLElement, number> = new WeakMap();

export function adjustTranslation(elem: HTMLElement, amount: number): void {
    const oldOffset = translations.get(elem) ?? 0;
    const newTranslateY = oldOffset + amount;
    if (newTranslateY < 0) {
        console.error("New transform is negative:", newTranslateY, elem);
        debugger;
        throw new Error("New transform is negative");
    }
    setTranslation(elem, newTranslateY);
}

export function setTranslation(elem: HTMLElement, offset: number): void {
    translations.set(elem, offset);
    elem.style.transform = `translateY(${(toNonExponentString(offset))}px)`;
}

export function removeTranslation(elem: HTMLElement): void {
    elem.style.removeProperty("transform");
    translations.delete(elem);
}

function toNonExponentString(number: number): string {
    const str = `${number}`;
    if (str.indexOf('e') === -1) {
        return str;
    }
    const digits = Math.log10(number);
    return number.toPrecision(Math.floor(digits + 1));
}
