const BUFFER_RATIO = 5;
export type Position = "above_buffer" | "above_viewport" | "visible" | "below_viewport" | "below_buffer";

export let getViewportPosition: (node: HTMLElement) => Position = getViewportPositionFactory();
window.addEventListener("resize", () => getViewportPosition = getViewportPositionFactory());

function getViewportPositionFactory() {
    const windowHeight = window.innerHeight;
    const bufferHeight = windowHeight * BUFFER_RATIO;
    return (elem: HTMLElement): Position  => {
        const bufferTop    = -bufferHeight;
        const bufferBottom = windowHeight + bufferHeight;
        const windowTop    = 0;
        const windowBottom = windowHeight;
        const bounds       = elem.getBoundingClientRect();
        const nodeTop      = bounds.y;
        const nodeBottom   = nodeTop + bounds.height;

        if (nodeBottom < bufferTop) {
            return "above_buffer";
        }
        if (nodeTop > bufferBottom) {
            return "below_buffer";
        }
        if (nodeBottom < windowTop) {
            return "above_viewport";
        }
        if (nodeTop > windowBottom) {
            return "below_viewport";
        }
        return "visible";
    };
}
