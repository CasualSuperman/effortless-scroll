
export type LifecyclePhase = "startup" | "active" | "aborted" | "shutdown";

interface BaseState {
    readonly phase: LifecyclePhase,
    readonly running: boolean,
}

interface StartupState<T> extends BaseState {
    readonly phase: "startup",
    readonly running: false,

    abort(): AbortedState<T>,
    ready(state: T): ActiveState<T>,
}

interface AbortedState<T> extends BaseState {
    readonly phase: "aborted",
    readonly running: false,
}

interface ShutdownState<T> extends BaseState {
    readonly phase: "shutdown",
    readonly running: false,

    restart(): StartupState<T>,
}

interface ActiveState<T> extends BaseState {
    readonly phase: "active",
    readonly running: true,
    readonly state: T,

    abort(e: Error): AbortedState<T>,
    shutdown(): ShutdownState<T>,
}

export type ExtensionState<T> = StartupState<T> | AbortedState<T> | ActiveState<T> | ShutdownState<T>;

const startupImpl = {
    phase: "startup",
    running: false,
    abort() {
        return abortImpl;
    },
    ready<T>(state: T) {
        return ready(state);
    },
} as const;

const abortImpl = {
    phase: "aborted",
    running: false,
    startup() {
        return startupImpl;
    },
    ready<T>(state: T) {
        return ready(state);
    },
} as const;

const shutdownImpl = {
    phase: "shutdown",
    running: false,
    restart() {
        return startupImpl;
    },
} as const;

const activeMarkerImpl = {
    phase: "active",
    running: true,
    shutdown: shutdown,
    abort: abort,
} as const;

export function startup<T>(): StartupState<T> {
    return startupImpl;
}

export function shutdown<T>(): ShutdownState<T> {
    return shutdownImpl;
}

function abort<T>(): AbortedState<T> {
    return abortImpl;
}

function ready<T>(state: T): ActiveState<T> {
    return {
        ...activeMarkerImpl,
        state,
    }
}
