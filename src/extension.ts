import {EffortlessScrollStateImpl} from "./main";
import {ExtensionState, startup} from "./extensionState";

export interface Scroller {
    shutdown(): void,
}

class EffortlessScroll implements XKitExtension {
    #state: ExtensionState<Scroller> = startup();

    get running() {
        return this.#state.running;
    }

    async run(): Promise<void> {
        this.#state = startup();
        if (!XKit.interface.is_tumblr_page()) {
            this.#state = this.#state.abort();
            return;
        }
        await XKit.css_map.getCssMap();
        const where = XKit.interface.where();
        if (!where.dashboard && !where.likes) {
            // The user is not on the dashboard, lets quit.
            console.log("Not on dashboard or likes page, nothing to do");
            this.#state = this.#state.abort();
            return;
        }

        const postSel = XKit.css_map.keyToCss('listTimelineObject') || '.post';
        const posts = document.querySelectorAll(postSel);

        if (posts.length === 0) {
            console.log("Unable to find any posts, stopping");
            this.#state = this.#state.abort();
            return;
        }

        const postContainer = posts[0].parentNode;
        if (postContainer === null) {
            console.log("Somehow post container was removed from the DOM");
            this.#state = this.#state.abort();
            return;
        } else if (!(postContainer instanceof HTMLElement)) {
            console.log("Somehow post container was not an HTML element");
            this.#state = this.#state.abort();
            return;
        }

        XKit.tools.init_css("EffortlessScroll");
        this.#state = this.#state.ready(new EffortlessScrollStateImpl(postContainer, this.onError.bind(this)));

        // I don't know what this does, but it uses too much CPU and it's not like I'm distributing this anyway,
        //   so I'm just hardcoding removing it.
        XKit.interface.post_window_listener.set_listen = function() {/* Begone */};
    }

    onError(e: Error): void {
        if (this.#state.phase === "active") {
            this.deactivate(this.#state.state);
            this.#state = this.#state.abort(e);
            console.error("Error during scroller execution", e);
        }
    }

    deactivate(scroller: Scroller): void {
        XKit.tools.remove_css("EffortlessScroll");
        scroller.shutdown();
    }

    destroy(): void {
        if (this.#state.phase === "active") {
            this.deactivate(this.#state.state);
            this.#state = this.#state.shutdown();
        }
    }
}

XKit.extensions.EffortlessScroll = new EffortlessScroll();
