import {PostStore} from "./store";
import {PROCESSED_SELECTOR} from "./css";
import {getViewportPosition, Position} from "./position";

const TARGET_BUFFER_COUNT = 5;

// Track nodes and their last known position
// When a node transitions from one position type to another, execute appropriate handlers
export class PositionHandler {
    #container: HTMLElement;
    #store: PostStore;
    #observer?: IntersectionObserver;
    #elements = new Set<HTMLElement>();

    constructor(container: HTMLElement, store: PostStore) {
        this.#container = container;
        this.#store = store;
        this.reactivate();
    }

    public observe(elem: HTMLElement): void {
        this.#elements.add(elem);
        this.#observer?.observe(elem);
    }

    public unobserve(elem: HTMLElement): void {
        this.#elements.delete(elem);
        this.#observer?.unobserve(elem);
    }

    public deactivate(): void {
        this.#observer?.disconnect();
        this.#observer = undefined;
    }

    public reactivate(): void {
        this.#observer = new IntersectionObserver(this.onScroll.bind(this));
        this.#elements.forEach(e => this.#observer?.observe(e));
    }

    public shutdown(): void {
        this.deactivate();
        this.#elements.clear();
    }

    private onScroll(): void {
        const newScrollPositions = this.getScrollPositions();
        let above = 0;
        let below = 0;
        for (const position of newScrollPositions.values()) {
            if (position === "above_buffer") {
                above++;
            } else if (position === "below_buffer") {
                below++;
            }
        }
        while (above > TARGET_BUFFER_COUNT) {
            const elem = this.#container.firstElementChild;
            if (elem instanceof HTMLElement) {
                this.unobserve(elem);
                this.#store.above.pushTail(elem);
                above--;
            } else {
                above = TARGET_BUFFER_COUNT;
            }
        }
        while (above < TARGET_BUFFER_COUNT) {
            const elem = this.#store.above.popTail();
            if (elem) {
                above++;
                this.#container.insertBefore(elem, this.#container.firstElementChild);
                const first = this.#container.firstElementChild;
                if (first instanceof HTMLElement) {
                    this.observe(first);
                }
            } else {
                above = TARGET_BUFFER_COUNT;
            }
        }
        while (below > TARGET_BUFFER_COUNT) {
            const last = this.#container.lastElementChild;
            if (last instanceof HTMLElement) {
                this.unobserve(last);
                this.#store.below.pushHead(last);
                below--;
            } else {
                below = TARGET_BUFFER_COUNT;
            }
        }
        while (below < TARGET_BUFFER_COUNT) {
            const elem = this.#store.below.popHead();
            if (elem !== undefined) {
                below++;
                this.#container.appendChild(elem);
                const last = this.#container.lastElementChild;
                if (last instanceof HTMLElement) {
                    this.observe(last);
                }
            } else {
                below = TARGET_BUFFER_COUNT;
            }
        }
    }

    private getScrollPositions(): Map<HTMLElement, Position> {
        const map = new Map();
        const nodes = this.#container.querySelectorAll(PROCESSED_SELECTOR);
        const len = nodes.length;
        for (let i = 0; i < len; i++) {
            const node = nodes[i];
            if (node instanceof HTMLElement) {
                map.set(node, getViewportPosition(node));
            }
        }
        return map;
    }
}
