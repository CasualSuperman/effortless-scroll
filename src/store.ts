"use strict";

import {Buffer} from "./buffer";

interface PostStoreConstructor {
    prototype: PostStore,
    new(): PostStore,
}

export interface PostStore {
    readonly above: Buffer,
    readonly below: Buffer,
}

export const PostStore: PostStoreConstructor = class implements PostStore {
    readonly #above = new Buffer();
    readonly #below = new Buffer();

    public get above(): Buffer {
        return this.#above;
    }

    public get below(): Buffer {
        return this.#below;
    }
}
