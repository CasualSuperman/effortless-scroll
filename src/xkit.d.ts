

declare const XKit: {
    readonly css_map: {
        getCssMap(): Promise<{readonly [key: string]: readonly string[]}>,
        keyToCss(className: string): string
    }, // Copied from blacklist extension

    readonly interface: {
        is_tumblr_page(): boolean
        where(): Readonly<{
            activity: boolean,
            channel: boolean,
            dashboard: boolean,
            drafts: boolean,
            endless: boolean,
            explore: boolean,
            followers: boolean,
            following: boolean,
            inbox: boolean,
            likes: boolean,
            queue: boolean,
            search: boolean,
            tagged: boolean,
            user_url: string,
        }>,
        post_window_listener: {
            set_listen: unknown,
        },
    }, //  - Interact with the page
    readonly version: unknown, //  - Returns XKit framework version
    readonly flags: unknown, //  - Flags that are set on the XKit Control Panel
    readonly tools: {
        init_css(extensionId: string): void,
        remove_css(extensionId: string): void,
    }, //  - Utility functions like add/remove CSS, get blogs, etc
    readonly post_listener: {
        add(name: string, callback: () => void): void,
        remove(name: string): void,
    }, //  - Listens for new posts to manipulate
    readonly download: unknown, //  - Download files and extensions
    readonly browser: unknown, //  - Get information about user's browser
    readonly extensions: {[k: string]: XKitExtension}, //  - Semi-god object for all the installed extensions
    readonly installed: unknown, //  - Check for installed extensions
    readonly storage: unknown, //  - Store/retrieve information

    readonly window: unknown, //  - Display message dialogs
    readonly notifications: unknown, //  - Show notifications for the user
    readonly progress: unknown, //  - Manipulate progress bars
};


interface XKitExtension {
    readonly running: boolean,
    run(): void,
    destroy(): void,
}
