"use strict";

interface Margins {
    readonly top: number,
    readonly bottom: number,
    readonly left: number,
    readonly right: number,
}

interface Dimensions {
    readonly width: number,
    readonly height: number,
}

interface RelativePosition {
    readonly top: number,
    readonly bottom: number,
    readonly left: number,
    readonly right: number,
}

interface AbsolutePosition {
    readonly x: number,
    readonly y: number,
}

export interface Measurements {
    position: {
        relative: RelativePosition,
        absolute: AbsolutePosition,
    },
    size: {
        outer: Dimensions,
        inner: Dimensions,
    },
    margins: Margins,
}

export function getElementHeight(elem: HTMLElement): number {
    const style = window.getComputedStyle(elem);
    return elem.getBoundingClientRect().height + parseFloat(style.marginTop) + parseFloat(style.marginBottom);
}

type Elems = {
    readonly length: number,
    [index: number]: HTMLElement,
};

export function getMeasurements(elems: Elems): Measurements[] {
    const len = elems.length;
    const arr = new Array(len);
    for (let i = 0; i < len; i++) {
        arr[i] = getMeasurement(elems[i]);
    }
    return arr;
}

export function getMeasurement(elem: HTMLElement): Measurements {
    const style = window.getComputedStyle(elem);
    const bounds = elem.getBoundingClientRect();
    const inner = {
        x: bounds.x,
        y: bounds.y,
        top: bounds.top,
        bottom: bounds.bottom,
        left: bounds.left,
        right: bounds.right,
        height: bounds.height,
        width: bounds.width,
    };
    const margins: Margins = {
        top: parseFloat(style.marginTop),
        bottom: parseFloat(style.marginBottom),
        left: parseFloat(style.marginLeft),
        right: parseFloat(style.marginRight),
    };
    const outer: AbsolutePosition & Dimensions = // The relative positioning calculated by this is not valid.
        new DOMRectReadOnly(
        inner.x - margins.left,
        inner.y - margins.top,
        inner.width + margins.left + margins.right,
        inner.height + margins.top + margins.bottom
    );
    return {
        margins,
        position: {
            relative: inner,
            absolute: inner,
        },
        size: {
            inner: inner,
            outer: outer,
        },
    };
}

type EventNames = keyof HTMLElementEventMap;

type PossibleEvents<T extends HTMLElement, K extends EventNames[]> = {
    [e in EventNames]: HTMLElementEventMap[e]
}[K[number]]

declare global {
    interface AddEventListenerOptions {
        signal?: AbortSignal
    }
}

function listenOnce<T extends HTMLElement, K extends EventNames[]>(elem: T, listener: (this: HTMLElement, ev: PossibleEvents<T, K>) => unknown, ...names: K) {
    const abort = new AbortController();
    const newListener = function(this: HTMLElement, ev: PossibleEvents<T, K>) {
        abort.abort();
        listener.call(this, ev);
    };
    for (const name of names) {
        elem.addEventListener(name, newListener, {signal: abort.signal, once: true});
    }
}


function awaitLoaded(elem: HTMLElement) {
    let waitingFor = 0;
    const complete = () => waitingFor--;
    if (elem instanceof HTMLMediaElement) {
        listenOnce(elem, complete, "loadedmetadata", "error");
    } else if (elem instanceof HTMLImageElement) {
        if (!elem.complete) {
            listenOnce(elem, complete, "load", "error");
        }
    }
}
