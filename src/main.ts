"use strict";

import {PostStore} from "./store";
import {getMeasurement, getMeasurements, Measurements} from "./dom";
import {Scroller} from "./extension";
import {getViewportPosition} from "./position";
import {PositionHandler} from "./visibility";
import {adjustTranslation, removeTranslation, setTranslation, PROCESSED_SELECTOR, UNPROCESSED_SELECTOR} from "./css";

type Elems = {
    readonly length: number,
    [index: number]: HTMLElement,
};

const debug = true;

type ErrorHandler = (e: Error) => void;
const ERROR_HANDLING_DISABLED = () => {/* No-op during construction, set at end of constructor */}

export class EffortlessScrollStateImpl implements Scroller {
    #processed: Range = new Range();
    #container: HTMLElement;
    #errorHandler: ErrorHandler = ERROR_HANDLING_DISABLED;
    #store: PostStore = new PostStore();
    #lastMeasurements: WeakMap<HTMLElement, Measurements> = new WeakMap();

    #resizeObserver = new ResizeObserver(this.handleResize.bind(this));
    #containerObserver = new MutationObserver(this.childUpdates.bind(this));

    #positionHandler: PositionHandler;

    #processedChildHeight: number = 0;

    constructor(postContainer: HTMLElement, onAbort: (e: Error) => void) {
        this.#container = postContainer;
        this.#positionHandler = new PositionHandler(postContainer, this.#store);
        this.initialize();
        this.#errorHandler = onAbort;
    }

    private initialize(): void {
        const firstPost = this.#container.firstElementChild;
        if (firstPost === null) {
            throw new Error("Parent node is not mounted");
        }
        this.#processed.setStartBefore(firstPost);
        this.#processed.setEndAfter(firstPost);

        this.initializeContainer();
        this.updatePositioning(this.getUnprocessedChildren());
        this.#containerObserver.observe(this.#container, {childList: true});
    }

    private initializeContainer(): void {
        this.#container.style.position = "relative";
    }

    private childUpdates(mutations: MutationRecord[]): void {
        for (const mutation of mutations) {
            if (mutation.addedNodes.length > 0) {
                this.updatePositioning(this.getUnprocessedChildren());
                return;
            }
        }
    }

    public shutdown(): void {
        this.#resizeObserver.disconnect();
        this.#containerObserver.disconnect();
        this.#positionHandler.shutdown();
        this.resetChildren();
        this.resetParent();
        this.#errorHandler = ERROR_HANDLING_DISABLED;
    }

    private resetParent(): void {
        this.#container.style.removeProperty("min-height");
        this.#container.style.removeProperty("position");
    }

    private resetChildren(): void {
        const processed = this.getProcessedChildren();
        const len = processed.length;
        for (let i = 0; i < len; i++) {
            const child = processed[i];
            child.classList.remove("effortless-post");
            removeTranslation(child);
        }
        this.#processedChildHeight = 0;
    }

    private getProcessedChildren(): Elems {
        return this.#container.querySelectorAll(PROCESSED_SELECTOR);
    }

    private getUnprocessedChildren(): HTMLElement[] {
        const elems = this.#container.querySelectorAll<HTMLElement>(UNPROCESSED_SELECTOR);
        const result: HTMLElement[] = [];
        for (let i = 0; i < elems.length; i++) {
            if (elems[i].clientHeight === 0) {
                //console.log("Removing ad:", elems[i]);
                elems[i].remove();
            } else {
                result.push(elems[i]);
            }
        }
        return result;
    }

    private updatePositioning(children: Elems): void {
        if (children.length > 0) {
            this.#positionHandler.deactivate();
            const parent = getMeasurement(this.#container);
            const measurements = getMeasurements(children);
            if (debug) {
                children[0].classList.add("first-in-page");
                children[children.length - 1].classList.add("last-in-page");
            }

            const toBuffer: [HTMLElement, Measurements][] = [];
            const len = measurements.length;
            for (let i = 0; i < len; i++) {
                const child = children[i];
                const m = measurements[i];
                // All nodes we are interacting with are not absolutely positioned yet, so the top of the first one
                //   should have a top of 0, and the rest should be positioned after it in standard flow.
                // This means their "true" position is that offset plus the total height of the children we've
                //   removed from standard flow.
                const finalOffset = (m.position.absolute.x - parent.position.absolute.x) + this.#processedChildHeight;
                //console.log("Post has offset of", finalOffset, "and height of", m.size.outer.height, child);
                this.#processedChildHeight += m.size.outer.height;
                if (finalOffset < 0) {
                    console.error("Initial transform is negative:", finalOffset, child);
                    debugger;
                    throw new Error("Transform is negative");
                }
                child.classList.add("effortless-post");
                setTranslation(child, finalOffset);
                const position = getViewportPosition(child)
                if (position === "below_buffer") {
                    toBuffer.push([child, m]);
                } else {
                    if (position === "above_buffer") {
                        throw new Error("New post somehow above buffer");
                    }
                    this.#lastMeasurements.set(child, m);
                    this.#resizeObserver.observe(child);
                    this.#positionHandler.observe(child);
                }
            }
            toBuffer.sort((a, b) => a[0].compareDocumentPosition(b[0]));
            for (const item of toBuffer) {
                const elem = item[0];
                this.#store.below.pushTail(elem);
                this.#lastMeasurements.set(elem, item[1]);
                this.#resizeObserver.observe(elem);
                this.#positionHandler.observe(elem);
            }
            this.#container.style.minHeight = `${this.#processedChildHeight}px`;
            this.#positionHandler.reactivate();
        }
    }

    private handleResize(entries: ResizeObserverEntry[], observer: ResizeObserver) {
        try {
            this.#positionHandler.deactivate();
            let scrollAmt = 0;
            for (const entry of entries) {
                const child = entry.target;
                if (child.ownerDocument !== window.document) {
                    continue;
                }
                if (!(child instanceof HTMLElement)) {
                    throw new Error("Observed element was of the wrong type");
                }
                const lastMeasurements = this.#lastMeasurements.get(child);
                if (lastMeasurements === undefined) {
                    throw new Error("Observed element was missing old measurements");
                }
                const newMeasurements = getMeasurement(child);
                this.#lastMeasurements.set(child, newMeasurements);
                const heightDifference = newMeasurements.size.outer.height - lastMeasurements.size.outer.height;
                if (heightDifference !== 0) {
                    if (newMeasurements.position.absolute.y + newMeasurements.size.outer.height < 0) {
                        scrollAmt += heightDifference;
                    }
                    //console.log("Post height changed by", heightDifference, child);
                    this.#processedChildHeight += heightDifference;
                    let nextChild = child.nextElementSibling;
                    while (nextChild instanceof HTMLElement) {
                        adjustTranslation(nextChild, heightDifference);
                        nextChild = nextChild.nextElementSibling;
                    }
                    this.#store.below.adjustTranslate(heightDifference);
                }
            }
            if (scrollAmt !== 0) {
                window.scrollBy({
                    top: scrollAmt,
                });
            }
            this.#container.style.minHeight = `${this.#processedChildHeight}px`;
            this.#positionHandler.reactivate();
        } catch (e) {
            this.#errorHandler(e);
        }
    }
}
