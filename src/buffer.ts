"use strict";

import {getElementHeight} from "./dom";
import {adjustTranslation} from "./css";

interface DetachedPost {
    readonly height: number,
    readonly holder: HTMLTemplateElement,
}

function detach(elem: HTMLElement): DetachedPost {
    const height = getElementHeight(elem);
    const holder = document.createElement("template");
    holder.content.appendChild(elem);
    return {
        height,
        holder,
    };
}

function adjustFragment(detached: DetachedPost, amount: number): void {
    const root = detached.holder.content.firstElementChild;
    if (root instanceof HTMLElement) {
        adjustTranslation(root, amount);
    } else {
        throw new Error("Holder root was not an element");
    }
}

interface BufferConstructor {
    prototype: Buffer,
    new(): Buffer,
}

export interface Buffer {
    readonly elem: Readonly<HTMLElement>,
    readonly height: number,
    adjustTranslate(amount: number): void,
    pushHead(elem: HTMLElement): void,
    pushTail(elem: HTMLElement): void,
    popHead(): DocumentFragment | undefined,
    popTail(): DocumentFragment | undefined,
}

export const Buffer: BufferConstructor = class implements Buffer {
    readonly #elem: HTMLElement = document.createElement("div");
    readonly #detached: DetachedPost[] = [];

    #totalHeight: number = 0;
    #forgotten: number = 0;

    public get elem(): Readonly<HTMLElement> {
        return this.#elem;
    }

    public get height(): number {
        return this.#totalHeight;
    }

    public adjustTranslate(amount: number): void {
        this.#detached.forEach(d => adjustFragment(d, amount));
    }

    public pushHead(elem: HTMLElement): void {
        this.#detached.unshift(this.toDetached(elem));
    }

    public pushTail(elem: HTMLElement): void {
        this.#detached.push(this.toDetached(elem));
    }

    private toDetached(elem: HTMLElement): DetachedPost {
        const post = detach(elem);
        this.adjustHeight(post.height);
        return post;
    }

    public popHead(): DocumentFragment | undefined {
        const item = this.#detached.shift();
        return this.postPop(item);
    }

    public popTail(): DocumentFragment | undefined {
        const item = this.#detached.pop();
        return this.postPop(item);
    }

    private postPop(item: DetachedPost | undefined): DocumentFragment | undefined {
        if (item) {
            this.adjustHeight(-item.height);
            return item.holder.content;
        }
    }

    private adjustHeight(amount: number): void {
        this.#totalHeight += amount;
        this.#elem.style.height = this.#totalHeight + "px";
    }
}
