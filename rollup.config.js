import typescript from '@rollup/plugin-typescript';

export default {
    input: './src/extension.ts',
    output: {
        file: 'dist/extension.js',
        format: 'iife',
        //sourcemap: 'inline',
        banner: `//* TITLE EffortlessScroll **//
//* VERSION 1.0.0 **//
//* DESCRIPTION Improves the performance of the dashboard by hiding posts you can't see **//
//* DEVELOPER CasualSuperman **//
//* FRAME false **//
//* BETA true **//`,
    },
    plugins: [typescript({
        module: "ESNext",
        noEmitOnError: true,
    })],
};
